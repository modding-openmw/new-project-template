# MOMW Mod Template

A template for building and distributing Morrowind mods, designed to be used as a base for new projects to quickly get going with.

#### Installation

1. Figure out what your mod should be! This is not at all a technical process, but before fully utilizing this template your mod should at least have a name and description.
2. Make sure you have a GitLab account, as the template is entirely focused around the use of GitLab static sites. You can have your own website for every mod! Just re-use this template as needed.
3. When you're ready to get started, scroll up, and hit the `fork` button in the top-right of the screen.
4. Before doing *anything else*, look to the left-hand side of your mod's repo page. On the left, there's a button, `Deploy`. Click that, then `Pages`. *Un*check the `use unique domain` option, and save your changes. Your site will be very broken if you don't do this! But don't worry, if you forget to do it (or don't feel like it yet), it can be saved for later. 

Unices:
1. Clone your new repository, or download it from this page by clicking the blue `Code` button, then `zip` (or your preferred format)
2. `cd` into the directory you extracted, or cloned, the repository into.

Windows:
1. If you have [git bash](https://git-scm.com/download/win) installed, feel free to skip this category.
2. If not, the easiest way to proceed is to use GitPod! Don't worry, this is easier than you may think. But keep in mind that you'll ultimately need Git installed, to update your mod and website. Unfortunately this isn't really optional.
3. From the repo page, hit `Edit`, and then `Use GitPod`. Follow the onscreen instructions (click `continue` a few times)
4. You should now be in VSCode, in a remote GitLab instance. You're given 10 free hours a month to work in GitPod, which should be more than enough to get started. Of course, to test your mod, you'll need one of a million code editors, but this is just to get you started.

Now then, everybody should all be in a shell, in their mod's root directory. If you're not, refer back to above. If that doesn't help, hit us up in Discord and we'll get you sorted.
From here, simply execute the following command:
`./fork_tool.sh`

The fork tool will ask for your GitLab account name, mod name, and mod description. Feel free to be elaborate. The fork tool will rename necessary folders and personalize the template website *just for you*.

Once it's done, it's almost time to start hacking! Enter one last command before you *really* get started: `git commit -am`. Make up whatever message you want here, but remember this marks the beginning of your mod's history on GitLab. Don't be *too* silly. :)

At this point, your mod is ready for packaging and working on. Code away, or plug in your existing files, and you're almost ready to rock - and show off your cool new mod website to everyone. 

For any files you add, use the command `git add`, but many code editors will also offer easier graphical options for this.

If needed, update the `LICENSE` file. The default provided license is MIT, but you're more than welcome to make it whatever. Other free license include GPL, Creative Commons, Beerware, and WTFPL.

Also make sure to update the `VERSION` file whenever you change the mod version.

Whenever you need to update the mod and send it off to GitLab, use the `git push` command.

Sit around for a bit... and you're done! Your mod is now published to GitLab, with a download link, and a public website everyone can see. 

The URL should be: https://YOUR_GITLAB_USERNAME.gitlab.io/YOUR_MOD_NAME

Share it around, have some fun, and tell your friends about the template!

This documentation will be destroyed after running the fork tool, so if you need to refer back to it, check out the original template repo. 

#### Report A Problem

If you've found an issue with this template, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/momw-mod-template/-/issues)
* Contact the team on Discord: `https://discord.gg/openmw-260439894298460160`

#### Credits

Author: **MOMW Contributors**
