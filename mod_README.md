# MOMW Mod Template

A template for building and distributing Morrowind mods, designed to be used as a base for new projects to quickly get going with. Made to be used with [this modders' guide](https://modding-openmw.com/guides/modders/).

#### Credits

Author: **Your Name Here**

#### Installation

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/momw-mod-template/)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\momw-mod-template

        # Linux
        /home/username/games/OpenMWMods/momw-mod-template

        # macOS
        /Users/username/games/OpenMWMods/momw-mod-template

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\momw-mod-template"`)
1. Add `content=momw-mod-template.omwaddon` and `content=momw-mod-template.omwscripts` to your load order in `openmw.cfg` or enable them via OpenMW-Launcher

#### Report A Problem

If you've found an issue with this mod, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/momw-mod-template/-/issues)
* Email `YOUR EMAIL HERE`
* Contact the author on Discord: `@YOUR DISCORD HERE`
